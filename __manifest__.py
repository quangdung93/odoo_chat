# -*- coding: utf-8 -*-
{
    'name': 'Hita Chat',
    'category': 'Themes',
    'author': 'PhatTai <tai.nguy3nphat@gmail.com>',
    'depends': ['product'],
    'data': [
        'views/views.xml',
        'views/menu.xml',
        'views/sticker.xml',
        'security/ir.model.access.csv',
    ], 
    'qweb': [
        'static/src/xml/hita_chat.xml',
    ],   
    'installable': True,
    'application': True,
}
