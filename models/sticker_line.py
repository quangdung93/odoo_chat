# -*- coding: utf-8 -*-
from dataclasses import field
from importlib.metadata import requires
from tokenize import String
from odoo import api, fields, models, _

class StickersLine(models.Model):
    _name = "stickers.line"

    zalo_ids = fields.Many2one('stickers.zalo', String="Stickers Line")
    key_cid = fields.Char(String="Key", required=True)
    id_zalo_external_id = fields.Char(String="Zalo External ID", required=True)
    url_image = fields.Char(String="Url Image", required=True)
    