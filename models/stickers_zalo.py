# -*- coding: utf-8 -*-
from email.policy import default
from importlib.metadata import requires
import string
from odoo import api, fields, models, _
import requests, json

class Stickers(models.Model):
    _name = "stickers.zalo"

    zalo_cid = fields.Char(string="Zalo ID", required=True)
    zalo_sticker_name = fields.Char( string="Sticker Name", required=True )
    zalo_sticker_url = fields.Char( string="Zalo Url Source", default="https://stickers.zaloapp.com/cate-stickers" )
    stickers_line_ids = fields.One2many('stickers.line','zalo_ids', string="Stickers")

    def button_crawl(self): 
        for record in self:
            data = requests.get(record.zalo_sticker_url, params={'cid': record.zalo_cid})
            jsonData = data.json()
            for item in jsonData['value']:
                sticker_ids = self.env['stickers.line'].search([('id_zalo_external_id', '=', item['id']), ('key_cid', '=', self.zalo_cid )] )
                print("---------------------------")
                print(len(sticker_ids))
                if len(sticker_ids) == 0:
                    self.env['stickers.line'].create({
                        'zalo_ids': self.id,
                        'key_cid' : self.zalo_cid,
                        'id_zalo_external_id' : item['id'],
                        'url_image' : item['url']
                    })
