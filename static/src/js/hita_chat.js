// odoo.define('hita_chat.main', 
    // function (require) {
        // const AbstractAction = require('web.AbstractAction');  
        // const core = require('web.core');
        const urlEmojiIcon = "../static/src/images/";
        const DOMAIN = "https://chathub.hita.com.vn";
        // const DOMAIN = "https://zalo-api.test";
        const HITA_AVATAR = 'https://s160-ava-talk.zadn.vn/1/3/7/3/4/160/fa71459de593194cd112bd2cb90ae275.jpg';
        const urlSuffix = {
            listMessageRecent: "messages-recent",
        };

        const API = {
            sendMessage: `${DOMAIN}/api/send-message`,
            reactMessage: `${DOMAIN}/api/react-message`,
            listMessages: `${DOMAIN}/api/messages`,
            listRooms: `${DOMAIN}/api/list-rooms`,
            uploadImage: `${DOMAIN}/api/upload-image`,
            uploadFile: `${DOMAIN}/api/upload-file`,
        }

        const CURRENT_USER = {
            element: null,
            name: '',
            uid: null,
            roomId: null,
            avatar: ''
        };

        // # PhatTai: Ham nay su dung tren view odoo, de lay thong tin session hien tai
        // const session = require('web.session');
        // # PhatTai: mau cac truong co ban cua 1 session
        const odoo_session = {
            company_id: 1, 
            is_admin: true,
            is_system: true,
            partner_display_name: "Lê Võ Trung Hiếu - CÔNG TY TNHH NỘI THẤT HITA",
            partner_id: 106,
            server_version: "14.0+e",
            name: 'Lê Võ Trung Hiếu', /* tên hiển thị dùng khi thao tác, lưu log*/
            uid: 69, /* id để quản lý người dùng hệ thống */
            username: "hieulee", /* username đăng nhập */
        }

        const ADMIN_NAME = 'Admin';
        const ADMIN_ID = 1;
        
        // const OurAction = AbstractAction.extend({  
        const OurAction = {  
            template: "hita_chat.ClientAction", 
            listReactEmoji: {
                like: '/-strong',
                heart: '/-heart',
                fun: ':>',
                surprise: ':o',
                cry: ':-((',
                angry: ':-h',
            },
            activeRoomId: 0,
            start: function(){
                var self = this,
                    body = $(document);
                
                body.on("mouseenter", ".message-container",function(){
                    self._mouseEmoi($(this).find('.more'), "mouseenter");
                })
                body.on("mouseleave", ".message-container",function(){
                    self._mouseEmoi($(this).find('.more'), "mouseleave");
                })
                body.on("click", elementDOM.reactionEmojiIcon, function () { 
                    self._actionEmoi(this);
                })

                //Image
                body.on('click', '.btn-upload-image', function(e){
                    e.preventDefault();
                    let file_upload = $(this).closest('.upload-container').find('.input-file');
                    file_upload[0].click();
                });
            
                body.on('change','.image.input-file',function () {
                    var files = $(this)[0].files,
                    userId = CURRENT_USER.uid,
                    
                    listFiles = [];

                    for(let i = 0; i < files.length; i++){
                        if(files[i].size > 1020000){ 
                            alert(`Dung lượng ảnh (${files[i].name}) không được vượt quá 1Mb`)
                            return;
                        }else{
                            listFiles.push(files[i]);
                        }
                    }
                    
                    if(listFiles.length > 0) self._handleImageUpload(listFiles, userId);
                });

                //File
                body.on('click', '.btn-upload-file', function(e){
                    e.preventDefault();
                    let file_upload = $(this).closest('.attach-container').find('.input-file');
                    file_upload[0].click();
                });

                body.on('change','.file.input-file',function () {
                    var files = $(this)[0].files,
                    userId = CURRENT_USER.uid,
                    
                    listFiles = [];

                    for(let i = 0; i < files.length; i++){
                        if(files[i].size > 1020000){ 
                            alert(`Dung lượng file (${files[i].name}) không được vượt quá 1Mb`)
                            return;
                        }else{
                            listFiles.push(files[i]);
                        }
                    }
                    
                    if(listFiles.length > 0) self._handleFileUpload(listFiles, userId);
                });

                //Click send message
                body.on("click", elementDOM.buttonSend, function(){
                    self._sendMessage();
                })

                //Enter send message
                body.on("keypress", elementDOM.txtArea, function(e){
                    var code = (e.keyCode ? e.keyCode : e.which);

                    if (code == 13) {
                        e.preventDefault();
                        self._sendMessage();
                    }
                })

                body.on("keyup", elementDOM.inputSearch, function(e){
                    let keyword = e.currentTarget.value;

                    if(keyword.length > 0 && keyword.length < 3){
                        return;
                    }

                    setTimeout(() => {
                        self._getListRoom(keyword);
                    }, 1000);
                })

                body.on("click", elementDOM.buttonQuote, function(){
                    self._actionQuote(this);
                })
                body.on("click", elementDOM.buttonRemove, function(){
                    self._removeQuote(this);
                })
                body.on("click", elementDOM.imgOnItem, function () { 
                    self._actionZoomImageMessage(this);
                })
                body.on("click", elementDOM.closeModal, function () { 
                    self._actionCloseModal(this);
                })
                body.on("click", elementDOM.btnDownload, function(){
                    self._downloadResource($(this).data("src"));
                })
                body.on("click", ".fade", function(){
                    self._actionCloseModal(this);
                })
                body.on("click", elementDOM.emojiSelectBoxChat, function(e){
                    e.stopPropagation();
                    self._actionAddEmojiBoxMessage($(this).data("key"));
                })
                body.on("click", elementDOM.emojiContainer, function(e){
                    e.preventDefault();
                    $(elementDOM.popover).toggle();
                })
                body.on("click", elementDOM.sectionPost, function(){
                    $(elementDOM.popover).hide();
                })
                body.on("click", elementDOM.sectionPost, function(){
                    $(elementDOM.popover).hide();
                })
                body.on("click", elementDOM.txtArea, function(){
                    $(elementDOM.popover).hide();
                })

                //Click room
                body.on("click", elementDOM.roomItem, function(){
                    $(".section-compose").show();
                    $(elementDOM.sectionPost).html("");

                    CURRENT_USER.element = $(this);
                    CURRENT_USER.name = $(this).data("name");
                    CURRENT_USER.uid = $(this).data("uid");
                    CURRENT_USER.roomId = $(this).data("room");
                    CURRENT_USER.avatar = $(this).data("avatar");

                    $(elementDOM.headingInfo).find(".user-current").remove();
                    $(elementDOM.headingInfo).prepend(self._renderUserCurrent(CURRENT_USER));

                    $("#user-action").show();
                    self._getListMessages();
                })

                body.on("click", elementDOM.itemStickerZl, function(){
                    let keyObject = $(this).data("key"),
                        arrayStickers = Stickers[keyObject],
                        html = '';
                    arrayStickers.forEach(record => {
                        html += self._renderHtmlSticker(Object.keys(record)[0], Object.values(record)[0]);
                    });
                    $(".emoji-preview").html(html);
                })
                body.on("click", elementDOM.stickersItem, function(){
                    let key = $(this).data("key");
                    let uid = $(elementDOM.headingInfo).find(".user-current").data("uid");
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': odoo.csrf_token
                        },
                        type: "POST",
                        url: ENDPOINT.sendMessage,
                        data: {
                            type: "photo",
                            user_id: uid,
                            message: "https://zalo-api.zadn.vn/api/emoticon/sticker/webpc?eid=46958",
                            quote_message_id: ""
                        },
                        context: self,
                        success: function (response) {
                            
                        }
                    });
                })

                body.on("click", ".action-orther", function(){
                    alert(1);
                })

                // render list partner onload page
                body.ready(function() {
                    self._renderEmojiListOnLoad();
                    self._getListRoom();
                    // self.getUserInfo();
                    self._renderStickersOnLoad();

                    var channel = pusher.subscribe('user.' + USER_ID);
                        channel.bind('App\\Events\\ChatEvent', function(data) {
                            console.log('socket-in', data);
                            self.onloadSocket(data);
                    });
                    
                });
            },

            //////********* Business function *************/////////

            onloadSocket: function(data){
                this._getListRoom();

                let message = data.message;

                if(message.reacted && this.activeRoomId == message.room_id){
                    this._getListMessages();
                }

                else if(this.activeRoomId == message.room_id){
                    $(elementDOM.sectionPost).append(this._renderItemMessage(message));
                }

                this._scrollBottomMessage();
            },

            _getListRoom: function(keyword = ''){
                let self = this;
                $.ajax({
                    type: 'GET',
                    url: API.listRooms,
                    data: {keyword, offset: 0, limit: 100},
                    dataType: 'json',
                    success: function (response) {
                        self._renderListRooms(response);
                    }
                });
            },

            _getListMessages: function(){
                let self = this;
                $.ajax({
                    type: "POST",
                    url: API.listMessages,
                    data: {room_id: CURRENT_USER.roomId, offset: 0, limit: 50},
                    dataType: "json",
                    success: function (response) {
                        let messages = response.data.messages;
                        self._renderListMessages(messages);

                        //Remove unseen icon
                        CURRENT_USER.element.find('.noti-count').remove();

                        //Set active room id
                        self.activeRoomId = CURRENT_USER.roomId;

                        //Scroll to bottom
                        self._scrollBottomMessage();
                    }
                });
            },

            _sendMessage: function(content = ''){
                let self = this;
                let value = this._getValueTextarea(elementDOM.textareaValue),
                    replyId = $(elementDOM.textareaValue).data('reply');
                    uid = $(elementDOM.headingInfo).find(".user-current").data("uid"),
                    roomId = $(elementDOM.headingInfo).find(".user-current").data("room");
                $.ajax({
                    // headers: {
                    //     'X-CSRF-TOKEN': odoo.csrf_token
                    // },
                    type: "POST",
                    url: API.sendMessage,
                    data: {
                        type: "text",
                        user_id: uid,
                        room_id: roomId,
                        message: content ? content : value,
                        quote_message_id: replyId ? replyId : null,
                        odoo_id: odoo_session.uid,
                        odoo_name: odoo_session.name,
                    },
                    success: function (response) {
                        
                        $(elementDOM.textareaValue).focus();

                        //Remove reply box
                        if(replyId){
                            self._removeAllQuote();
                        }

                        //Reload messages
                        self._getListMessages();

                        //Clear text
                        self._clearTextarea(elementDOM.textareaValue);

                        //Scroll to bottom
                        self._scrollBottomMessage();
                    }
                });
            },

            _sendMessageImageFile: function(content, filePath, type = 'image'){
                let self = this;
                let uid = $(elementDOM.headingInfo).find(".user-current").data("uid"),
                    roomId = $(elementDOM.headingInfo).find(".user-current").data("room");
                $.ajax({
                    // headers: {
                    //     'X-CSRF-TOKEN': odoo.csrf_token
                    // },
                    type: "POST",
                    url: API.sendMessage,
                    data: {
                        type: type,
                        user_id: uid,
                        room_id: roomId,
                        message: content,
                        file_path: filePath,
                        quote_message_id: null,
                        odoo_id: odoo_session.uid,
                        odoo_name: odoo_session.name,
                    },
                    success: function (response) {
                        //Reload messages
                        self._getListMessages();

                        //Scroll to bottom
                        self._scrollBottomMessage();
                    }
                });
            },

            _renderListRooms: function(data){
                let html = '';
                    data.data.forEach(record => {
                        html += this._renderItemRoom(record);
                    });

                $(elementDOM.partnerBody).html(html);
            },

            _renderItemRoom: function(item){
                let message = '';
                let type = item.last_message.type;
                let lastMessage = item.last_message;

                if(type == "image"){
                    message = "[Hình ảnh]";
                }
                else if(type == "file"){
                    message = "[File]";
                }
                else if(type == "nosupport"){
                    message = "[Cuộc hội thoại]";
                }
                else if(type == "sticker"){
                    message = "[Sticker]";
                }
                else if(type == "link"){
                    message = "[Link]";
                }
                else if(item.last_message.content){
                    message += lastMessage.author_id == ADMIN_ID ? 'Hita: ' : '';
                    message += this._regexEmojiMessage(item.last_message.content);
                }

                let messageUnseen = item.message_unseen == 1 ? '<span class="noti-count"></span>' : '';

                return `
                    <div class="item" id="${item.id}" 
                    data-uid="${item.user_id}" 
                    data-room="${item.id}"
                    data-name="${item.author.name}" 
                    data-avatar="${item.author.avatar}">
                        <div class="avatar">
                            <img src="${item.author.avatar}" />
                        </div>
                        <div class="snippet">
                            <div class="partner-name">${item.author.name}</div>
                            <div class="message truncate">
                                <span class="message-content">${message}</span>
                                ${messageUnseen}
                            </div>
                            <div class="item-timestamp">${Helper.convertTime(item.updated_at)}</div>
                        </div>
                    </div>
                `;
            },

            //Render list message
            _renderListMessages: function(data){
                let lastTime = 0;
                let currentChat = 0;
                data.forEach(item => {

                    if(item.author_id == currentChat && Helper.checkAgoTime(item.created_at, lastTime) < 1800){
                        $(elementDOM.sectionPost)
                            .find(".section-item")
                            .eq(-1)
                            .find(".snippet")
                            .append(this._renderMessageItemContent(item));
                    }
                    else{
                        if(Helper.checkAgoTime(item.created_at, lastTime) > 1800){
                            let timeText = Helper.renderSectionTimestamp(item.created_at);
                            $(elementDOM.sectionPost).append(timeText);
                        }

                        $(elementDOM.sectionPost).append(this._renderItemMessage(item));
                    }

                    lastTime = item.created_at;
                    currentChat = item.author_id;
                });
            },

            //Render message item
            _renderItemMessage : function(data){
                let avatar = data.author_id == ADMIN_ID ? HITA_AVATAR : data.author.avatar;
                let authorName = data.author_id == ADMIN_ID ? `OA: ${data.admin_name || ADMIN_NAME}` : data.author.name;

                return `<div class="section-item ${data.author_id == ADMIN_ID ? "me" : ""}">
                    <div class="avatar avatar--sm">
                        <img src="${avatar}" />
                    </div>
                    <div class="item-content">
                        <div class="item-headline-container">
                            <div class="item-headline">
                                ${authorName}
                            </div>
                            <div class="snippet">
                                ${this._renderMessageItemContent(data)}
                            </div>
                            <div class="section-ts">
                                <span>${Helper.convertTime(data.updated_at)}</span>
                            </div>
                        </div>
                    </div>
                </div>
                `;
            },

            _renderMessageItemContent : function(value){
                let imageUrl = '',
                    messageHtml = '';
                
                if(value.type == 'text'){
                    if(value.reply){
                        let quoteName = value.reply.admin_name;
                        let quoteContent = value.reply.content;
                        messageHtml = this._renderQuoteMessage(quoteName, quoteContent);
                    }
                    
                    messageHtml += `<span>${this._regexEmojiMessage(value.content)}</span>`;
                }
                else if(value.type == 'image' || value.type == 'sticker'){
                    if(!value.content.includes('http')){
                        imageUrl = `${DOMAIN}/storage/${value.content}`
                    }
                    else{
                        imageUrl = value.content;
                    }

                    messageHtml = `<img width="100%" src="${imageUrl}" />`;
                }
                else if(value.type == 'file'){
                    let fileUrl = value.content;
                    let objectFile = fileUrl.split('/');
                    let fileFullName = objectFile.at(-1);
                    let objFileFullName = fileFullName.split('.');
                    let fileName = objFileFullName[0];
                    let fileExtension = objFileFullName[1];
                    let iconPath = '';
                    
                    if(fileExtension == 'pdf'){
                        iconPath = '../static/src/images/icon-pdf.png';
                    }
                    else if(fileExtension == 'docx' || fileExtension == 'doc'){
                        iconPath = '../static/src/images/icon-word.png';
                    }
                    else if(fileExtension == 'xls' || fileExtension == 'xlsx'){
                        iconPath = '../static/src/images/icon-excel.png';
                    }

                    messageHtml = `<img width="30px" src="${iconPath}" />`;
                    messageHtml += `<a href="${fileUrl}" target="_blank" style="margin-left: 10px">${fileFullName}</a>`;
                }
                else if(value.type == 'link'){
                    let content = value.content;
                    let objectFile = content.split('/');
                    messageHtml = `<a href="${value.content}" target="_blank">${value.content}</a>`;
                }

                let checkIsAdmin = value.author_id == ADMIN_ID ? true : false;

                let moreItem = !checkIsAdmin ? this._renderHtmlMoreItem() : '';
                let iconReact = !checkIsAdmin ? this._renderReactActionEmojiDefault() : '';

                let activeReactIcons = value.react ? value.react.split(',') : [];
                activeReactIcons = activeReactIcons.filter(n => n);

                let activeEmojiClass = '';
                let htmlActiveReactIcon = '';
                if(activeReactIcons.length > 0){
                    activeEmojiClass = 'active-emoji';
                    let htmlItemIcon = '';
                    let listReactEmoji = this.listReactEmoji;
                    let iconExist = [];
                    activeReactIcons.forEach(item => {
                        let key = Object.keys(listReactEmoji).find(k => listReactEmoji[k] === item);

                        if(!iconExist.includes(key)){
                            htmlItemIcon += `<div class="reaction-emoji-icon"><i class="emoi ${key}" data-react="${item}"></i></div>`;
                        }

                        iconExist.push(key);
                    })
                    htmlActiveReactIcon = this._renderShowEmoji(htmlItemIcon, activeReactIcons.length);

                    let lastActiveEmoji = activeReactIcons.pop();
                    let lastActiveIconClass = Object.keys(listReactEmoji).find(k => listReactEmoji[k] === lastActiveEmoji);
                    htmlActiveReactIcon += this._renderReactActiveActionEmoji(lastActiveIconClass, lastActiveEmoji);
                }
                
                return `
                    <div class="admin-message-wrapper">
                        <div class="message-container">
                            <div class="item-message ${value.type} phanhoi ${activeEmojiClass}" data-id="${value.id}">
                                <span>${messageHtml}</span>
                                ${moreItem}
                                ${htmlActiveReactIcon || iconReact}
                            </div>
                        </div>
                    </div>
                `;
            },

            _actionQuote: function(element){
                // $(elementDOM.sectionPost).addClass('quote-show');
                this._removeAllQuote();
                let dataQuote = {
                    name: this._getNameQuote(element),
                    messageQuote: this._getTextQuote(element)
                };
                let html = this._renderQuoteContent(dataQuote);
                $(elementDOM.txtArea).prepend(html);

                //Gán message id vào textarea
                let messageId = $(element).closest('.item-message').data('id');
                $(elementDOM.textareaValue).attr('data-reply', messageId);
            },

            _renderQuoteContent: function(dataQuote){
                return `
                    <div class="preview_mess">
                        <i class="icon_bar icon_close"></i>
                        <div class="wrap">
                            <div class="content_get"></div>
                            <div class="content_mess">
                                <div class="reply_name">
                                    <i class="icon_bar icon_quote_black"></i>
                                    <span>Trả lời <strong>${dataQuote.name}</strong></span>
                                </div>
                                <div class="reply_text">${dataQuote.messageQuote}</div>
                            </div>
                        </div>
                    </div>
                `;
            },

            _renderQuoteMessage: function(name, content){
                return `
                    <div class="preview_mess">
                        <div class="wrap">
                            <div class="content_get"></div>
                            <div class="content_mess">
                                <div class="reply_name">
                                    <i class="icon_bar icon_quote_black"></i>
                                    <span><strong>${name}</strong></span>
                                </div>
                                <div class="reply_text">${content}</div>
                            </div>
                        </div>
                    </div>
                `;
            },

            _actionEmoi: function (element) {
                let rootItem = $(element).closest(elementDOM.itemMessage);
                let selectedIcon = $(element).data('react');
                let messageId = rootItem.data('id');
                
                rootItem.addClass("active-emoji");

                $(element).closest(".react_emoi").find(".icon_emoi").html(this._renderIconActive($(element)[0]));

                if(rootItem.find(elementDOM.showEmoji).length == 0){
                    rootItem.append(this._renderShowEmoji(this._renderReactionEmoji($(element)[0]), 1));
                }
                else{
                    let classIcon = this._getClassListActionEmoi($(element)[0]);
                    if(rootItem.find(elementDOM.icEmoji).find(classIcon).length == 0){
                        this._checkCountEmoiActive(rootItem);

                        // add icon show
                        rootItem.find(elementDOM.icEmoji).find(".count").before(this._renderReactionEmoji($(element)[0],1));
                    }
                    // update count emoi action
                    let count = rootItem.find(".count").html();
                    count++;
                    rootItem.find(".count").html(count);
                }

                //Call api
                $.ajax({
                    type: "POST",
                    url: API.reactMessage,
                    data: {
                        user_id: CURRENT_USER.uid,
                        react_icon: selectedIcon,
                        react_message_id: messageId
                    },
                    success: function (response) {
                        
                    }
                });
            },

            _renderIconActive: function(icon){
                return `<div class="icon-active">${icon.outerHTML}</div>`;
            },

            _renderShowEmoji: function(icon, count){
                return `
                    <div class="show-emoji">
                        <div class="ic_emoji">
                            ${icon}
                            <span class="count">${count}</span>
                        </div>
                    </div>
                `;
            },

            _renderReactionEmoji: function(icon){
                return `
                    <div class="reaction-emoji-icon">
                        ${icon.outerHTML}
                    </div>
                `;
            },

            _renderReactActionEmojiDefault: function() {
                return `<div class="react_emoi">
                    <div class="icon_emoi"><i class="emoi icon_heart"></i></div>
                    <div class="list_emoi">
                        <div class="ic_emoji">
                            ${this._renderListIcon()}
                            <span class="close"></span>
                        </div>
                    </div>
                </div>`;
            },

            _renderReactActiveActionEmoji: function(className, emoji) {
                return `<div class="react_emoi">
                    <div class="icon_emoi">
                        <div class="icon-active">
                            <i class="emoi ${className}" data-react="${emoji}"></i>
                        </div>
                    </div>
                    <div class="list_emoi">
                        <div class="ic_emoji">
                            ${this._renderListIcon()}
                            <span class="close"></span>
                        </div>
                    </div>
                </div>`;
            },

            _getClassListActionEmoi: function (element) { 
                let classIcon = "";
                element.classList.forEach(ele => {
                    if(typeof ele !== void 0){
                        classIcon += `.${ele}`;
                    }
                })
                return classIcon;
            },

            _checkCountEmoiActive: function(rootItem){
                let eleIcEmoji = rootItem.find(elementDOM.icEmoji),
                countItemActive = eleIcEmoji.find(elementDOM.reactionEmojiIconDiv).length;

                if( countItemActive >= 3 ){
                    eleIcEmoji.find(elementDOM.reactionEmojiIconDiv).eq(0).remove();
                }
            },

            //////********* End Business function *************/////////

            ///////*********  Helper function ***************//////

            _regexEmojiMessage: function(text){
                for(var i = 0; i< Object.values(regexEmoji).length; i++){
                    let emojiImg = document.createElement("img");
                    emojiImg.classList.add("emoji-img");
                    emojiImg.src = urlEmojiIcon.concat(Object.values(regexEmoji)[i]);
                    text = text.replaceAll(Object.keys(regexEmoji)[i], emojiImg.outerHTML);
                }
                return text;
            },


            _renderSectionMessageSocket: function(data){
                let template = `
                    <div class="section-item">
                        <div class="avatar avatar--sm">
                            <img src="${data.message.sender.avatar}" />
                        </div>
                        <div class="item-content">
                            <div class="item-headline-container">
                                <div class="item-headline">
                                    ${data.message.sender.name}
                                </div>
                                <div class="snippet">
                                    ${this._renderMessageItemSocket(data)}
                                </div>
                                <div class="section-ts">
                                    <span>${Helper.convertTime(parseInt(data.message.timestamp))}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
                    return template;
            },
            _renderMessageItemSocket : function(data){
                return `
                    <div class="admin-message-wrapper">
                        <div class="message-container">
                            <div class="item-message text phanhoi">
                                <span>
                                    <span>${this._regexEmojiMessage(data.message.message.text)}</span>
                                </span>
                                ${this._renderHtmlMoreItem()}
                                <div class="react_emoi">
                                    <div class="icon_emoi"><i class="emoi icon_heart"></i></div>
                                    <div class="list_emoi">
                                        <div class="ic_emoji">
                                            ${this._renderListIcon()}
                                            <span class="close"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            },
            _renderItemRecentConversationSocket: function(data){
                return `
                    <div class="item" id="${data.message.sender.id}" data-uid="${data.message.sender.id}" data-name="${data.message.sender.name}" data-avatar="${data.message.sender.avatar}">
                        <div class="avatar">
                            <img src="${data.message.sender.avatar}" />
                        </div>
                        <div class="snippet">
                            <div class="partner-name">${data.message.sender.name}</div>
                            <div class="message truncate">
                                <span>${this._regexEmojiMessage(data.message.message.text)}</span>
                            </div>
                            <div class="item-timestamp">${Helper.convertTime(parseInt(data.message.timestamp))}</div>
                        </div>
                    </div>
                `;
            },
            onMessage: function(data){
                console.log("tsst");
            },

            getUserInfo: function(user_id){
                var user;
                $.ajax({
                    type: "GET",
                    url: "https://chathub.hita.com.vn/api/messages-by-user",
                    data: {user_id: user_id},
                    dataType: "json",
                    async: false,
                    success: function (response) {
                        let dataFilter = response.data.filter((item) => item.src > 0);
                        user = {
                            avatar: dataFilter[0].from_avatar,
                            display_name: dataFilter[0].from_display_name
                        }
                    }
                });
                return user;
            },
            _renderHtmlSticker: function (id, imgId) {  
                return `
                    <li class="sticker-item" data-key="${id}">
                        <span> <img src="${this._renderStickerById(imgId)}" /> </span>
                    </li>
                `;
            },
            _renderStickersOnLoad: function(){
                let listKeyStickers = Object.keys(Stickers);
                listKeyStickers.forEach( record => {
                    $("#list-stickers").append(this._renderStickerItem(record, this._renderStickerById(Object.values(Stickers[record][0])[0])));
                });
            },
            _renderStickerItem: function(key,img){
                return `<li data-key="${key}">
                            <span class="item-sticker">
                                <img src="${img}" width="30" height="30"/>
                            </span>
                        </li>`;
            },
            _renderStickerById: function(id){
                return `https://zalo-api.zadn.vn/api/emoticon/sticker/webpc?eid=${id}`;
            },
            _scrollBottomMessage: function(){
                let objScroll = document.getElementById('section-post');
                objScroll.scrollTop = objScroll.scrollHeight;
            },
            _actionAddEmojiBoxMessage: function(emojiText){
                $(elementDOM.textareaValue).val(function(i, text){
                    return text + emojiText;
                })
            },
            _renderEmojiListOnLoad: function(){
                let html = ``;
                for(var i = 0; i< Object.values(regexEmoji).length; i++){
                    let emojiImg = document.createElement("img");
                    emojiImg.classList.add("emoji-img");
                    emojiImg.src = urlEmojiIcon.concat(Object.values(regexEmoji)[i]);
                    html += `<li> <span class="item-emoji" data-key="${Object.keys(regexEmoji)[i]}">${emojiImg.outerHTML}</span></li>`;
                }
                $(elementDOM.emojiPreview).html(html);
                
            },
            _actionCloseModal: function(ele){
                $(ele).closest(".modal-container").removeClass("show");
                $(ele).closest(".modal-container").find(".data-body").html("");
            },
            _getFileName: function(urlFile){
                return urlFile.split('/').pop();
            },
            _forceDownload: function (blob, filename) {
                var a = document.createElement('a');
                a.download = filename;
                a.href = blob;
                document.body.appendChild(a);
                a.click();
                a.remove();
            },

            // Current blob size limit is around 500MB for browsers
            _downloadResource: function (url, filename) {
            if (!filename) filename = url.split('\\').pop().split('/').pop();
            fetch(url, {
                headers: new Headers({
                    'Origin': location.origin
                }),
                mode: 'cors'
                })
                .then(response => response.blob())
                .then(blob => {
                let blobUrl = window.URL.createObjectURL(blob);
                this._forceDownload(blobUrl, filename);
                })
                .catch(e => console.error(e));
            },
            _actionZoomImageMessage: function(ele) {
                let img = document.createElement('img'),
                    rootItem = $(document).find(elementDOM.modalZoomImage);
                    img.src = $(ele)[0].src;
                rootItem.addClass("show");
                rootItem.find(".data-body").html(img);
                rootItem.find('.btn-download').attr('data-src', img.src);
                rootItem.find('.btn-download').attr('data-name', this._getFileName(img.src));
            },
            _removeQuote: function(ele){
                $(ele).closest(".preview_mess").remove();
            },

            _removeAllQuote: function(){
                $(elementDOM.txtArea).find(".preview_mess").remove();
            },
            _getTextQuote: function(element){
                return $(element).closest(elementDOM.itemMessage).text().trim();
            },
            _getNameQuote: function(element){
                return $(element).closest(elementDOM.itemContent).find(elementDOM.itemPartnerName).text().trim();
            },
            
            _getValueTextarea: function(element){
                return $(element).val();
            },
            _clearTextarea: function (element) { 
                $(element).val("");
            },
            _mouseEmoi : function(element, action){
                action == "mouseenter" ? element.addClass("show") : element.removeClass("show");
            },

            _renderListIcon: function(){
                let html = ``;
                Object.keys(this.listReactEmoji).forEach(key => {
                    html += `<div class="reaction-emoji-icon"><i class="emoi ${key}" data-react="${this.listReactEmoji[key]}"></i></div>`;
                })
                return html;
            },
            _renderHtmlMoreItem: function(){
                return `
                    <div class="more">
                        <div class="inner">
                            <div class="btn_quote">
                                <i class="icon_bar icon_quote"> </i>
                            </div>
                            <div class="btn_dot group-dot">
                                <i class="icon_bar icon_dot"> </i>
                            </div>
                        </div>
                    </div>
                `;
            },

            _renderUserCurrent: function(userItem){
                return `<div class="user-current" 
                    data-uid="${userItem.uid}" 
                    data-room="${userItem.roomId}" 
                    data-user-name="${userItem.name}" 
                    data-avatar="${userItem.avatar}">
                        <div class="avatar">
                            <img src="${userItem.avatar}" />
                        </div>
                        <div class="snippet">
                            <div class="item-title">
                                <span class="nickname">${userItem.name}</span>
                                <span class="edit-nickname">
                                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" height="12" viewBox="0 0 16 16" width="12">
                                        <path d="M8.29289 3.70711L1 11V15H5L12.2929 7.70711L8.29289 3.70711Z" fill="#030708"/>
                                        <path d="M9.70711 2.29289L13.7071 6.29289L15.1716 4.82843C15.702 4.29799 16 3.57857 16 2.82843C16 1.26633 14.7337 0 13.1716 0C12.4214 0 11.702 0.297995 11.1716 0.828428L9.70711 2.29289Z" fill="#030708"/>
                                    </svg>
                                </span>
                            </div>
                            <div class="container-name-tag">
                                <div class="exact-name">
                                <span>Tên Zalo: ${userItem.name} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                `;
            },

            _handleImageUpload: function(files, userId){
                let self = this;
                var form_data = new FormData();
        
                for(let i = 0; i < files.length; i++){
                    form_data.append('images[]', files[i]);
                }
        
                form_data.append('user_id', userId);
        
                $.ajax({
                    type: "POST",
                    url: API.uploadImage,
                    data: form_data,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if(!response.error){
                            let imagesUploadeds = response.data;
                            if(imagesUploadeds.length > 0){
                                imagesUploadeds.forEach(async(item) => {
                                    await self._sendMessageImageFile(item.attachment_id, item.path);
                                })
                            }
                        }
                        else{
                            alert(response.message.sub_message);
                        }
                    },
                    error: function (n) {
                        console.log(`Error: ${JSON.stringify(n)}`);
                    }
                });
            },

            _handleFileUpload: function(files, userId){
                let self = this;
                var form_data = new FormData();
        
                for(let i = 0; i < files.length; i++){
                    form_data.append('files[]', files[i]);
                }
        
                form_data.append('user_id', userId);
        
                $.ajax({
                    type: "POST",
                    url: API.uploadFile,
                    data: form_data,
                    async: true,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if(!response.error){
                            let imagesUploadeds = response.data;
                            if(imagesUploadeds.length > 0){
                                imagesUploadeds.forEach(async(item) => {
                                    await self._sendMessageImageFile(item.token, item.path, 'file');
                                })
                            }
                        }
                        else if(response.error == 2){ //File excel
                            self._sendMessage(response.data.path);
                        }
                        else{
                            alert(response.message.sub_message);
                        }
                    },
                    error: function (n) {
                        console.log(`Error: ${JSON.stringify(n)}`);
                    }
                });
            }
            

        }
        // });
        
        // core.action_registry.add('hita_chat.action', OurAction);
    // }
// );                    
