const Helper = {

    checkAgoTime: function(time, oldTime){
        const timestamp = new Date(time);
        const oldTimestamp = new Date(oldTime);
        let timeParse = timestamp.getTime();
        let oldTimeParse = oldTimestamp.getTime();
        return Math.floor((timeParse - oldTimeParse) / 1000);
    },

    readURL: function(input, element) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                element.attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    },

    updateTimestamp: function(){
        return new Date().toLocaleTimeString('en-GB', { hour: "numeric", minute: "numeric"});
    },

    renderSectionTimestamp: function (time) {
        let gzTime = new Date(time),
            clock = gzTime.toLocaleTimeString('en-GB', { hour: "numeric", minute: "numeric"}),
            timeText = `${listDate[gzTime.getDay()]}, ${gzTime.getDate()} tháng ${listMonth[gzTime.getMonth()]} năm ${gzTime.getFullYear()}`;
        if(gzTime.toLocaleDateString() == (new Date()).toLocaleDateString()){
            timeText = "Hôm nay";
        }
        return `
            <div class="section-timestamp">
                <span>${clock} ${timeText} </span>
            </div>
        `;
    },

    convertTime: function(time){
        var dataTime = new Date(time),
            nowDate = new Date(),
            DateDiff = {

                inDays: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();

                    return Math.floor((t2-t1)/(24*3600*1000));
                },
                inWeeks: function(d1, d2) {
                    var t2 = d2.getTime();
                    var t1 = d1.getTime();
                    return parseInt((t2-t1)/(24*3600*1000*7));
                },
                inMonths: function(d1, d2) {
                    var d1Y = d1.getFullYear();
                    var d2Y = d2.getFullYear();
                    var d1M = d1.getMonth();
                    var d2M = d2.getMonth();
                    return (d2M+12*d2Y)-(d1M+12*d1Y);
                },

                inYears: function(d1, d2) {
                    return d2.getFullYear()-d1.getFullYear();
                }
            };
        if(DateDiff.inDays(dataTime, nowDate) == 0){
            return dataTime.toLocaleTimeString('en-GB', { hour: "numeric", minute: "numeric"});
        }
        else if(DateDiff.inDays(dataTime, nowDate) == 1){
            return "Hôm qua";
        }
        else if(DateDiff.inDays(dataTime, nowDate) < 7){
            return listDate[dataTime.getDay()];
        }
        else {
            return dataTime.toLocaleDateString('en-GB');
        }
        
    },

    timeSince: function(timeStamp, type = 'default') {

        if (!(timeStamp instanceof Date)) {
            timeStamp = new Date(timeStamp);
        }

        if (isNaN(timeStamp.getDate())) {
            return "Invalid date";
        }

        var seconds = Math.floor((new Date() - timeStamp) / 1000);
    
        var interval = seconds / 31536000;
        
        switch(type){
            case 'today':
                if (interval > 1 || seconds / 2592000 > 1 || seconds / 86400 > 1) {
                    return moment(timeStamp).format('DD/MM/yyyy - hh:mm A');
                }
        
                interval = seconds / 3600;
                if (interval > 1) {
                    return Math.floor(interval) + " giờ trước";
                }
                interval = seconds / 60;
                if (interval > 1) {
                    return Math.floor(interval) + " phút trước";
                }
        
                return seconds > 0 ? Math.floor(seconds) + " giây trước" : 1 + " giây trước";  
            default:
                if (interval > 1) {
                    return Math.floor(interval) + " năm";
                }
                interval = seconds / 2592000;
                if (interval > 1) {
                    return Math.floor(interval) + " tháng";
                }
                interval = seconds / 86400;
                if (interval > 1) {
                return Math.floor(interval) + " ngày";
                }
        
        
                interval = seconds / 3600;
                if (interval > 1) {
                    return Math.floor(interval) + " giờ";
                }
                interval = seconds / 60;
                if (interval > 1) {
                    return Math.floor(interval) + " phút";
                }
        
                return seconds > 0 ? Math.floor(seconds) + " giây" : 1 + " giây";  
        }
    },
}